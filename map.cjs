function map(elements = [], cb) {
  let result = []

  if (Array.isArray(elements) && typeof (cb) === 'function') {

    for (let index=0; index < elements.length; index++) {

      result.push(cb(elements[index], index, elements))
    }
  }
  return result;
}

module.exports = map;