function filter(elements = [], cb) {
  let result = [];
  if (Array.isArray(elements) && typeof (cb) === 'function') {

    for (let index in elements) {
      if (cb(elements[index], index, elements) == true) {
        result.push(elements[index]);
      }
    }
  }
  return result;
}

module.exports = filter;