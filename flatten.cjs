function flatten(elements = [], depth = 1) {
  let result = [];
  if (Array.isArray(elements)) {

    for (let index = 0; index < elements.length; index++) {

      if (Array.isArray(elements[index]) && depth != 0) {

        result = result.concat(flatten(elements[index], depth - 1))

      } else if ((elements[index]) !== undefined) {
        result.push(elements[index]);
      }
    }
  }
  return result;
}
//const nestedArray = [1, [2], [[3]], [[[4]]]];
//console.log(flatten(nestedArray));
module.exports = flatten;