function each(elements=[], cb) {
  if(Array.isArray(elements) == true && typeof(cb) === 'function') {

    for(let index in elements) {

      cb(elements[index], index);

    }
  }
};

//let a = each(element, cb);
//console.log(a);
module.exports = each;