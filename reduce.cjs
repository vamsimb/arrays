function reduce(elements = [], cb, startingValue) {

  if (Array.isArray(elements) && typeof (cb) === 'function') {

    if (startingValue == undefined) {
      startingValue = elements[0]
      for (let index = 1; index < elements.length; index++) {
        startingValue = cb(startingValue, elements[index], index, elements)
      }
    }
    else {
      for (let index = 0; index < elements.length; index++) {
        startingValue = cb(startingValue, elements[index], index, elements)
      }
    }
    return startingValue
  }
  return []
}

module.exports = reduce;