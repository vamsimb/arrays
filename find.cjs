function find(elements=[], cb) {

  const filter = require('./filter.cjs')

  if(filter(elements, cb)[0]) return filter(elements, cb)[0];

  return [];
  
}

module.exports = find;